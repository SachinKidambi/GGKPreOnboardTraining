﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimeFibonacci
{
    class Pattern
    {
        int Input()
        {
            Console.WriteLine("Enter input");
            return Int32.Parse(Console.ReadLine());
        }
        bool IsPrime(int num)
        {
            if(num==1)
            {
                return false;
            }
            for(int i=2;i<Math.Sqrt(num);i++)
            {
                if(num%i==0)
                {
                    return false;
                }
            }
            return true;
        }
        string[] PrimeFibonacciArray(int num)
        {
            int firstNum = 0;
            int secondNum = 1;
            string primeFeb = "";
            for (int i = 0; i < num; i++)
            {
                int temp = firstNum + secondNum;
                firstNum = secondNum;
                secondNum = temp;
                if(IsPrime(temp))
                {
                    primeFeb = primeFeb + " "+ temp;
                }
            }
            return primeFeb.Split(' ');
        }
        void Display(string[] primeFebArray)
        {
            for(int i=0;i<primeFebArray.Length;i++)
            {
                for(int j=0;j<=i;j++)
                {
                    Console.Write(primeFebArray[j] + " ");
                }
                Console.WriteLine();
            }
        }
        static void Main(string[] args)
        {
            Pattern obj = new Pattern();
            obj.Display(obj.PrimeFibonacciArray(obj.Input()));
            Console.ReadKey();
        }
    }
}
