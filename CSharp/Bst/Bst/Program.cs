﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bst
{
    class Node
    {
        public int data;
        public Node left, right;
        public Node(int data)
        {
            this.data = data;
        }
    }
    class Program
    {
        int n;
        Node root;
        void Input()
        {
            Console.WriteLine("Enter the number of nodes");
            n = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Enter the data of each node follwed by ENTER");
            for (int i = 0; i < n; i++)
                Insert(root, Int32.Parse(Console.ReadLine()));
        }
        void Insert(Node root, int data)
        {
            if (root == null)
                this.root = new Node(data);
            else if (root.data > data)
            {
                if (root.left == null)
                    root.left = new Node(data);
                else
                    Insert(root.left, data);
            }
            else
            {
                if (root.right == null)
                    root.right = new Node(data);
                else
                    Insert(root.right, data);
            }
        }
        void InOrder(Node root)
        {
            if (root == null)
                return;

            InOrder(root.left);
            Console.Write(root.data + " ");
            InOrder(root.right);
        }
        void RevOrder(Node root)
        {
            if (root == null)
                return;

            RevOrder(root.right);
            Console.Write(root.data + " ");
            RevOrder(root.left);
        }
        public static void Main(String[] args)
        {
            Program obj = new Program();
            obj.Input();
            obj.InOrder(obj.root);
            Console.WriteLine();
            obj.RevOrder(obj.root);
            Console.ReadLine();
        }
    }
}
