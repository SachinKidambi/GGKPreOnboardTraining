﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigSum
{
    class Program
    {
        String str1, str2;
        void Input()
        {
            Console.WriteLine("Enter the first number");
            str1 = Console.ReadLine();
            for (int i = 0; i < str1.Length; i++)
            {
                if (!(str1[i] >= 0 && str1[i] <= 9))
                {
                    Console.WriteLine("Invalid Input");
                    Console.WriteLine("Press any alphabet follwed by ENTER to exit");
                    Console.ReadLine();
                    System.Environment.Exit(0);
                }
            }
            Console.WriteLine("Enter the second number");
            str2 = Console.ReadLine();
            for (int i = 0; i < str2.Length; i++)
            {
                if (!(str2[i] >= 0 && str2[i] <= 9))
                {
                    Console.WriteLine("Invalid Input");
                    Console.WriteLine("Press any alphabet follwed by ENTER to exit");
                    Console.ReadLine();
                    System.Environment.Exit(0);
                }
            }

        }
        void GiveSum()
        {

            if (str1.Length < str2.Length)
            {
                int lenDiff = str2.Length - str1.Length;
                for (int i = 0; i < lenDiff; i++)
                    str1 = 0 + str1;
            }
            else
            {
                int lenDiff = str1.Length - str2.Length;
                for (int i = 0; i < lenDiff; i++)
                    str2 = 0 + str2;
            }
            String temp = "";
            int carry = 0;
            for (int i = str1.Length - 1; i >= 0; i--)
            {
                int sum = Int32.Parse(str1[i] + "") + Int32.Parse(str2[i] + "") + carry;
                temp = (sum % 10) + temp;
                carry = sum / 10;
            }
            if (carry == 1)
                temp = carry + temp;
            Console.WriteLine("Your output sum is = "+temp);
        }
        public static void Main(String[] args)
        {
            Program obj = new Program();
            obj.Input();
            obj.GiveSum();
            Console.ReadLine();
        }
    }
}
