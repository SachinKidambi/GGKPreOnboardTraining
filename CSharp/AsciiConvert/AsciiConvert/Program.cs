﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AsciiConvert
{
    class Program
    {
        String str;
        void Input()
        {
            Console.WriteLine("Enter the input ");
            str = Console.ReadLine();
            for(int i=0;i<str.Length;i++)
            {
                if (!((str[i] >= 'A' && str[i] <= 'Z') || (str[i] >= 'a' && str[i] <= 'z')))
                {
                    Console.WriteLine("Invalid Input");
                    Console.WriteLine("Press any alphabet follwed by ENTER to exit");
                    Console.ReadLine();
                    System.Environment.Exit(0);
                }
            }
        }
        void Convert()
        {
            for (int i = 0; i < str.Length - 1; i++)
            {
                int avg = (str[i] + str[i + 1]) / 2;
                if (IsPrime(avg))
                    avg++;
                Console.Write((char)avg);
            }
        }
        bool IsPrime(int k)
        {
            for (int i = 2; i < k; i++)
                if (k % i == 0)
                    return false;
            return true;
        }
        public static void Main(String[] args)
        {
            Program obj = new Program();
            obj.Input();
            obj.Convert();
            Console.ReadLine();
        }
    }
}
