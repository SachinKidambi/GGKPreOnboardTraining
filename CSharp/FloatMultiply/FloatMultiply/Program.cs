﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FloatMultiply
{
    class Program
    {
        float ReadInput()
        {
            Console.WriteLine("Enter input");
            return float.Parse(Console.ReadLine());
        }
        
        static void Main(string[] args)
        {
            Program p = new Program();
            MultiplyFloats obj = new MultiplyFloats();
            float result = obj.MultiplyTwoFloats(p.ReadInput(),p.ReadInput());
            Console.WriteLine(result);
        }
    }
}
