﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FloatMultiply
{
    class MultiplyFloats
    {
        int roundingNum;


        /// <summary>
        /// Takes two floats as inputs and computes it's product
        /// </summary>
        /// <param name="firstInput"></param>
        /// <param name="secondInput"></param>
        /// <returns>The product of the two inputs</returns>
        public float MultiplyTwoFloats(float firstInput,float secondInput)
        {
            

            roundingNum = FindingRoundingNum(firstInput, secondInput);

            //convert to binary
            string firstInputInBinary = Trim(ToBinary(firstInput));
            string secondInputInBinary = Trim(ToBinary(secondInput));

            //finding index of point after multiplying binary numbers
            int indexOfPoint = firstInputInBinary.Length - firstInputInBinary.IndexOf('.') + secondInputInBinary.Length - secondInputInBinary.IndexOf('.') - 2;

            firstInputInBinary = firstInputInBinary.Remove(firstInputInBinary.IndexOf('.'), 1);
            secondInputInBinary = secondInputInBinary.Remove(secondInputInBinary.IndexOf('.'), 1);

            //performing multiplication
            string multipiedResultInBinary = Multiply(firstInputInBinary, secondInputInBinary);

            //dividing result into integer part and decimal part for conversion
            int integerPart = Convert.ToInt32(multipiedResultInBinary.Substring(0, multipiedResultInBinary.Length - indexOfPoint), 2);
            float decimalPart = (float)Math.Round(ToDecimal(Trim("." + multipiedResultInBinary.Substring(multipiedResultInBinary.Length - indexOfPoint))), roundingNum);

            //result
            return (float)(integerPart + decimalPart);
        }


        double ToDecimal(string inputString)
        {
            inputString = inputString.Remove(0, 1);
            float sum = 0;
            for (int index = 0; index < inputString.Length; index++)
            {
                if (inputString[index] == '1')
                {
                    sum = sum + (float)Math.Pow(2, -(index + 1));
                    sum = (float)Math.Round(sum, roundingNum);
                }
            }
            return sum;
        }
        string ToBinary(float inputNum)
        {
            int integerPart = (int)inputNum;
            float decimalPart = inputNum - integerPart;
            decimalPart = (float)Math.Round(decimalPart, roundingNum);
            string integerPartInBinary = "";
            string decimalPartInBinary = "";
            while (integerPart > 0)
            {
                integerPartInBinary = (integerPart % 2) + integerPartInBinary;
                integerPart = integerPart / 2;
            }
            for (int count = 0; count < 25; count++)
            {
                decimalPart = (float)Math.Round(decimalPart, roundingNum);
                decimalPart = decimalPart * 2;
                decimalPart = (float)Math.Round(decimalPart, roundingNum);
                if (decimalPart > 1.0)
                {
                    decimalPartInBinary = decimalPartInBinary + "1";
                    decimalPart = decimalPart - 1;
                }
                else if (decimalPart < 1)
                {
                    decimalPartInBinary = decimalPartInBinary + "0";
                }
                else
                {
                    decimalPartInBinary = decimalPartInBinary + "1";
                    break;
                }
            }
            return integerPartInBinary + "." + decimalPartInBinary;
        }


        string Trim(string inputString)
        {
            while (inputString.Length > 1 && inputString[inputString.Length - 1] == '0')
            {
                inputString = inputString.Remove(inputString.Length - 1, 1);
            }
            while (inputString.Length > 1 && inputString[0] == '0')
            {
                inputString = inputString.Remove(0, 1);
            }
            return inputString;
        }
        int FindingRoundingNum(float firstNum, float secondNum)
        {
            String temp1 = firstNum + "";
            String temp2 = secondNum + "";
            return temp1.Substring(temp1.IndexOf('.') + 1).Length + temp2.Substring(temp2.IndexOf('.') + 1).Length;
        }



        string Multiply(string firstString, string secondString)
        {
            string result = "";
            for (int index = secondString.Length - 1; index >= 0; index--)
            {
                if (secondString[index] == '1')
                {
                    string temp = AddRequiredZeros(firstString, secondString.Length - index - 1);
                    if (result.Length == 0)
                    {
                        result = firstString;
                    }
                    else
                    {
                        result = AddBinary(result, temp);
                    }
                }
            }
            return result;
        }

        string AddRequiredZeros(string ans, int zeros)
        {
            for (int count = 0; count < zeros; count++)
            {
                ans = ans + "0";
            }
            return ans;
        }

        string AddBinary(string firstBinary, string secondBinary)
        {
            string result = "";
            int carry = 0;
            int lenDif = secondBinary.Length - firstBinary.Length;
            for (int count = 0; count < lenDif; count++)
            {
                firstBinary = "0" + firstBinary;
            }
            for (int index = 0; index < firstBinary.Length; index++)
            {
                int tempSum = Int32.Parse(firstBinary[firstBinary.Length - 1 - index] + "") + Int32.Parse(secondBinary[secondBinary.Length - 1 - index] + "") + carry;
                result = tempSum % 2 + result;
                carry = tempSum / 2;
            }
            if (carry == 1)
            {
                result = 1 + result;
            }
            return result;
        }
    }
}
