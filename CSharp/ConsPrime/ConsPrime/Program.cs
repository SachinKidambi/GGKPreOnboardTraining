﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsPrime
{
    class Program
    {
        int num;
        void Input()
        {
            Console.WriteLine("enter the input ");
            num = Int32.Parse(Console.ReadLine());
        }
        void CountPrimeSum()
        {
            int sum = 0, count = 0;
            for (int i = 2; i <= num; i++)
            {
                i = NextPrime(i);
                sum = sum + i;
                if (sum > num)
                    break;
                if (sum > 2 && IsPrime(sum))
                {
                    Console.WriteLine(sum);
                    count++;
                }
            }
            Console.WriteLine("Required output is = "+count);
        }
        int NextPrime(int k)
        {
            while (true)
            {
                if (IsPrime(k))
                    return k;
                else
                    k++;
            }
        }
        bool IsPrime(int k)
        {
            for (int i = 2; i < k; i++)
                if (k % i == 0)
                    return false;
            return true;
        }
        public static void Main(String[] args)
        {
            Program obj = new Program();
            obj.Input();
            obj.CountPrimeSum();
            Console.ReadLine();
        }
    }
}
