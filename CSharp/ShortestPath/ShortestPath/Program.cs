﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShortestPath
{
    class Program
    {
        private int[,] graph;
        private int n, src, destination;
        private int[] prevNode;
        private String[] nodeArr;
        public void Input()
        {
            Console.WriteLine("enter the no of nodes");
            n = Int32.Parse(Console.ReadLine());
            Console.WriteLine("enter the nodes separated by space");
            nodeArr = (Console.ReadLine()).Split(' ');
            graph = new int[n, n];
            Console.WriteLine("enter the number of connections");
            int k = Int32.Parse(Console.ReadLine());
            Console.WriteLine("enter the nodes to be connected followed by distance");
            for (int i = 0; i < k; i++)
            {
                String[] a = (Console.ReadLine()).Split(' ');
                int u = Array.IndexOf(nodeArr, a[0]);
                int v = Array.IndexOf(nodeArr, a[1]);
                int d = Int32.Parse(a[2]);
                graph[u, v] = d;
                graph[v, u] = d;
            }
            Console.WriteLine("enter the source");
            src = Array.IndexOf(nodeArr, Console.ReadLine());
            Console.WriteLine("enter the destination");
            destination = Array.IndexOf(nodeArr, Console.ReadLine());

        }
        public void Dijkstra(int[,] graph)
        {
            int[] distance = new int[n];
            bool[] isSet = new bool[n];
            prevNode = new int[n];
            for (int index = 0; index < n; index++)
            {
                distance[index] = int.MaxValue;
                isSet[index] = false;
                prevNode[index] = 0;
            }
            distance[src] = 0;
            for (int i = 0; i < n - 1; i++)
            {
                int temp = SetMin(distance, isSet);
                isSet[temp] = true;
                for (int index = 0; index < n; index++)
                {
                    if (isSet[index] == false && graph[temp, index] != 0 && distance[temp] != int.MaxValue && distance[temp] + graph[temp, index] <= distance[index])
                    {
                        distance[index] = distance[temp] + graph[temp, index];
                        prevNode[index] = temp;
                    }
                }
            }
            int tempIndex = destination;
            String str = "";
            while (tempIndex != src)
            {
                str = nodeArr[prevNode[tempIndex]] + "-->" + str;
                tempIndex = prevNode[tempIndex];
            }
            str = str + nodeArr[destination];
            Console.WriteLine("the shortest path between " + nodeArr[src] + " and " + nodeArr[destination] + " is " + str + " with distance of " + distance[destination]);
        }
        public int SetMin(int[] distance, bool[] isSet)
        {
            int tempMin = int.MaxValue, tempIndex = 0;
            for (int index = 0; index < n; index++)
            {
                if (isSet[index] == false && distance[index] < tempMin)
                {
                    tempMin = distance[index];
                    tempIndex = index;
                }
            }
            return tempIndex;
        }
        public static void Main(String[] args)
        {
            Program obj = new Program();
            obj.Input();
            obj.Dijkstra(obj.graph);
            Console.ReadLine();
        }
    }
}
